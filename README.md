English translation of Zelda - A Link to the Dream a fan game created by team Zeldaforce using the solarus engine.

This isn't an official translation from the team, but I plan to keep this up to date alongside development of Alttd and the team is welcome
to use this translation in their project.

To use, simply add the languages folder in this repo to the Alttd data folder. Then in the solarus editor, add the language to the project.

It's still a work in progress but as of now, 99% of the dialog from the french version has been translated.

Text from the original Link's Awakening was used where possible,for item descriptions and text not in the original, I tried to keep the tone
and style as close to the original games as possible.



